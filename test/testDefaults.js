const defaults = require('../defaults');
const obj = {
    name: 'John',
    age: undefined,
    city: 'New York',
};

const defaultProps = {
    age: 25,
    gender: 'Male',
    hasHome: 'true'
};

const result = defaults(obj, defaultProps);
console.log(result);