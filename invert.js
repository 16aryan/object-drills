function invert(obj) {
    // Returns a copy of the object where the keys have become the values and the values the keys.
    // Assume that all of the object's values will be unique and string serializable.
    // http://underscorejs.org/#invert
    if(!obj){
        return console.log('undefined object');
    }
    const invertedObject ={};
    for(let keys in obj){
       
        let temp = obj[keys];
        temp = JSON.stringify(temp);
        invertedObject[temp]=keys;
     
       
    }
    

    console.log(invertedObject);
  }
  
  const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
  invert(testObject);

module.exports = invert;
