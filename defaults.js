function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults
    if(!obj){
        return undefined;
    }

    for(let keys in defaultProps){
        if(obj[keys]=== undefined){
            obj[keys]=defaultProps[keys];
        }
       
        
    }
    return obj;
}



module.exports = defaults;
