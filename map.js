
function mapObject(obj, cb) {

    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject
    if(!obj){
        console.log('undefined objectt');
    }

    for(let keys in obj){
        if(typeof obj[keys]==='number'){
            obj[keys]= cb(obj[keys]);
            console.log(`updated element ${keys} : ${obj[keys]}`);
        }
    }
}
function cb(element){
    return element+5;
}


module.exports = {mapObject,cb};